//
//  ViewController.m
//  Photos
//
//  Created by administrator on 13-8-13.
//  Copyright (c) 2013年 Administrator. All rights reserved.
//

#import "ViewController.h"
#import "detailViewController.h"
@interface ViewController ()

@end

@implementation ViewController
{
    NSArray *recipes;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    recipes = [[NSArray alloc]initWithObjects:@"1.jpg",@"2.jpg",@"3.jpg",@"4.jpg",@"5.jpg",@"6.jpg",@"7.jpg",@"8.jpg",@"9.jpg",@"10.jpg" ,nil];
//
//    NSString *path = [[NSBundle mainBundle] pathForResource:@"name" ofType:@"plist"];
//    NSDictionary *dir = [[NSDictionary alloc] initWithContentsOfFile:path];
//    
//   recipes = [dir objectForKey:@"name"];
//    NSLog(@"%@",[[recipes objectAtIndex:0] class]);
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [recipes count];

}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"RecipeCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    cell.textLabel.text = [recipes objectAtIndex:indexPath.row];
    cell.imageView.image = [UIImage imageNamed:[recipes objectAtIndex:indexPath.row]];
    
    return cell;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//把当前的照片传到下一个页面显示
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"showRecipeDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        detailViewController *destViewController = segue.destinationViewController;
        destViewController.recipeName = [recipes objectAtIndex:indexPath.row];
    }
}

- (void)dealloc {
    [_tableView release];
    [super dealloc];
}
@end
