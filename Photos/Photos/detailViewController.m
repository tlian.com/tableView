//
//  detailViewController.m
//  Photos
//
//  Created by administrator on 13-8-14.
//  Copyright (c) 2013年 Administrator. All rights reserved.
//

#import "detailViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>
@interface detailViewController ()

@end

@implementation detailViewController
@synthesize recipeName,imgeView;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    imgStarWidth=imageView.frame.size.width;
	imgStarHeight=imageView.frame.size.height;
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self.imgeView setImage:[UIImage imageNamed:recipeName]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(id) initWithCoder:(NSCoder *)aDecoder
{
    lastDistance = 0;
    return [super initWithCoder:aDecoder];
}
-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    CGPoint p1;
    CGPoint p2;
    CGFloat sub_x;
    CGFloat sub_y;
    CGFloat currentDistance;
    CGRect imgFrame;
    NSArray *touchesArr = [[event allTouches]allObjects];
//    NSLog(@"手指个数 %i",[touchesArr count]);
    if ([touchesArr count] >= 2)
    {
        p1 = [[touchesArr objectAtIndex:0]locationInView:self.view];
        p2 = [[touchesArr objectAtIndex:1]locationInView:self.view];
        sub_x = p1.x - p2.x;
        sub_y = p1.y - p2.y;
        currentDistance = sqrtf(sub_x * sub_x + sub_y *sub_y);
        if (lastDistance > 0)
        {
            imgFrame = imageView.frame;
            if (currentDistance > lastDistance +2)
            {
//                NSLog(@"放大");
                imgFrame.size.width += 10;
                if (imgFrame.size.width > 1000)
                {
                    imgFrame.size.width = 1000;
                    
                }
                lastDistance = currentDistance;
                
            }
            if (currentDistance < lastDistance -2)
            {
//                NSLog(@"缩小");
                imgFrame.size.width -= 10;
                if (imgFrame.size.width < 50)
                {
                    imgFrame.size.width = 50;
                }
                lastDistance = currentDistance;
            }
            if (lastDistance == currentDistance)
            {
                imgFrame.size.height = imgStarHeight *imgFrame.size.width/imgStarWidth;
                float addwidth = imgFrame.size.width - imageView.frame.size.width;
                float addheight = imgFrame.size.height -imageView.frame.size.height;
                imageView.frame = CGRectMake(imgFrame.origin.x - addwidth/2.0f, imgFrame.origin.y - addheight/2.0f, imgFrame.size.width, imgFrame.size.height);
                
                
            }
            
        }
        else
        {
            lastDistance = currentDistance;
        }
        
        
    }
    
    
}
-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    lastDistance = 0;
}


- (void)dealloc {
    [imgeView release];
    [super dealloc];
}
- (IBAction)locaPhoto:(id)sender
{
    UIImagePickerController *picker = [[UIImagePickerController alloc]init];
    picker.delegate = self;
    [self presentViewController:picker animated:YES completion:nil];
    
    
    
    
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [self dismissViewControllerAnimated:YES completion:nil];
    UIImage *gotImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    if (imgeView.image != nil) {
        [imgeView release];
        imgeView.image = gotImage;
    }
    else{
        imgeView.image = gotImage;
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)save:(id)sender
{
    UIImageWriteToSavedPhotosAlbum(imgeView.image, self, @selector(imageSavedToPhotosAlbum:didFinishSavingWithError: contextInfo:), nil);
}
- (void)imageSavedToPhotosAlbum:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    NSString *message;
    NSString *title;
    if (!error) {
        title = NSLocalizedString(@"储存成功",@"");
        message = NSLocalizedString(@"您的图片正确的储存至本地相册", @"");
    }
    else {
    

    
        title = NSLocalizedString(@"储存失败", @"");
        message = [error description];
    }
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:title message:message delegate:nil cancelButtonTitle:@"好" otherButtonTitles:nil];
    [alert show];
    [alert release];
    [message release];
    [title release];
    
}













@end
