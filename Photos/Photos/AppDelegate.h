//
//  AppDelegate.h
//  Photos
//
//  Created by administrator on 13-8-13.
//  Copyright (c) 2013年 Administrator. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
